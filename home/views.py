from django.shortcuts import render

def index(request):
    return render(request, 'index.html')

def experience(request):
    return render(request, 'experience.html')

def education(request):
    return render(request, 'education.html')

def skills(request):
    return render(request, 'skills.html')

def contact(request):
    return render(request, 'contact.html')

def portofolio(request):
    return render(request, 'portofolio.html')