from django.urls import path
from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('experience/', views.experience, name='experience'),
    path('education/', views.education, name='education'),
    path('skills/', views.skills, name='skills'),
    path('contact/', views.contact, name='contact'),
    path('portofolio/', views.portofolio, name='portofolio'),
]